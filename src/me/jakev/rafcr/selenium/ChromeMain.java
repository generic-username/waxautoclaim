package me.jakev.rafcr.selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.io.File;
import java.time.Duration;
import java.util.List;

public class ChromeMain {
    private static final String ASSET_IDS = "[1099680377302,1099667043962,1099666986574,1099666675319,1099666674974,1099666674753,1099666674301,1099666661016,1099666660876,1099666660668,1099666660470,1099666660287,1099666660205,1099666829417,1099666668575,1099666668469,1099666668399,1099666668297,1099679398963,1099666684884,1099666681291,1099666681112,1099666680930,1099666679805, 1099680334330,1099679890721,1099676119508,1099666669776,1099666669635,1099710781126,1099710781438,1099667091514,1099671431393,1099671714473,1099671714535,1099667151843,1099667152480,1099675901187,1099666696531,1099667645246,1099666991547,1099680461754,1099666633864,1099688619881,1099688619745,1099670570437,1099666986746,1099666661049,1099666633979,1099666633914]";

    public static void main(String[] args) {
        new ChromeMain();
    }

    WebDriver driver;

    public ChromeMain() {
//        System.setProperty("webdriver.chrome.driver", new File(".").getAbsolutePath() + File.separator + "chromedriver");
        WebDriverManager.chromedriver().setup();
    }
    public void closeBrowser(){
        driver.quit();
    }
    public void login(){
        WebElement login = locateElementByName("Login");
        sleep(1000); //button doesnt work for a little (why implicitWait is not great)
        login.click();

        // Sometimes, a "Connect to wallet" prompt will appear, and we need to click Cloud Wallet for it to login.
        // This happens randomly
        // Calling findElements triggers the implicitWait for the max duration if none are found
        List<WebElement> elements = driver.findElements(By.xpath("//*[contains(text(),'Cloud Wallet')]"));
        if(!elements.isEmpty()){
            WebElement cWallet = elements.get(0);
            cWallet.click();
            sleep(1000);
        }
        WebElement account = locateElementByName(" hb4iu.wam (active) ");
        account.click();

        sleep(4000);
    }
    public void spawnBrowser() {
        boolean success = false;
        while (!success){
            try{
                ChromeOptions options = new ChromeOptions();
                options.addArguments("user-data-dir=" + new File(".sel").getAbsolutePath(), "user-agent=Chrome " + (Math.random() * 1000));
                driver = new ChromeDriver(options);
                driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
                driver.get("https://wax.bloks.io/account/anmworldgame?loadContract=true&tab=Actions&account=anmworldgame&scope=anmworldgame&limit=100");
                String title = driver.getTitle();
                System.out.println("WINDOW TITLE: " + title);
                success = true;
            }catch (Exception e){
                System.err.println("ERROR! could not spawn window, retrying...");
                e.printStackTrace();
                driver.close();
                sleep(1000);
            }
        }

    }
    public void buyAndUseBooster(){
        WebElement buyShop = locateElementByName("buyshopl");
        System.out.println(buyShop.getText());
        buyShop.click();

        // Input text of buyshopl
        WebElement nameTextInput = driver.findElement(By.cssSelector("input[placeholder='Enter account name...']"));
        nameTextInput.clear();
        nameTextInput.click();
        nameTextInput.sendKeys("hb4iu.wam");

        // Input text of id
        List<WebElement> numInputs = driver.findElements(By.cssSelector("input[placeholder='Enter number...']"));
        WebElement idInput = numInputs.get(0);
        WebElement quantityInput = numInputs.get(1);

        idInput.click();
        idInput.clear();
        idInput.sendKeys("7");

        quantityInput.click();
        quantityInput.clear();
        quantityInput.sendKeys("50");

        WebElement submitButton = driver.findElement(By.cssSelector("button[id='push-transaction-btn']"));
        submitButton.click();
        sleep(2000);

            //// USE BOOST

        WebElement useBoost = locateElementByName("useboost");
        System.out.println(useBoost.getText());
        useBoost.click();

        // Input text of useboost
        WebElement assetIdInput = driver.findElement(By.cssSelector("input[placeholder='Enter uint64[]...']"));
        assetIdInput.clear();
        assetIdInput.click();
        assetIdInput.sendKeys(ASSET_IDS);

        // Input type
        WebElement typeInput = driver.findElement(By.cssSelector("input[placeholder='Enter string...']"));
        typeInput.clear();
        typeInput.click();
        typeInput.sendKeys("tree");

        WebElement submitButtonB = driver.findElement(By.cssSelector("button[id='push-transaction-btn']"));
        submitButtonB.click();
        sleep(1000);
    }
    public enum ClaimTreeSubmissionError {
        CANNOT_CLAIM_YET,
        OTHER,
        NEEDS_FERTILIZER,
        NONE;
    }
    public ClaimTreeSubmissionError claimTreeSubmission(){
        // Click submit for claimtree
        WebElement submitButton = driver.findElement(By.cssSelector("button[id='push-transaction-btn']"));
        submitButton.click();

        List<WebElement> error = driver.findElements(By.cssSelector("div[class='ui error message']"));
        if(!error.isEmpty()){
            WebElement errorMsg = error.get(0).findElement(By.tagName("p"));
            String text = errorMsg.getText();
            System.out.println("[ChromeMain] <WARNING> Error message: " + text);
            if(text.contains("claim yet")){
                return ClaimTreeSubmissionError.CANNOT_CLAIM_YET;
            }else if(text.contains("fertilizer")){
                return ClaimTreeSubmissionError.NEEDS_FERTILIZER;
            }else{
                return ClaimTreeSubmissionError.OTHER;
            }
        }else{
            System.err.println("[ChromeMain] ClaimTree did not return error");
            return ClaimTreeSubmissionError.NONE;
        }
    }
    public ClaimTreeSubmissionError claimTreeTextInput(){
        // Click claimtree
        WebElement claimTree = locateElementByName("claimtree");
        System.out.println(claimTree.getText());
        claimTree.click();

        // Input text of claimtree
        WebElement nameTextInput = driver.findElement(By.cssSelector("input[placeholder='Enter account name...']"));
        nameTextInput.clear();
        nameTextInput.click();
        nameTextInput.sendKeys("hb4iu.wam");

        WebElement assetIdInput = driver.findElement(By.cssSelector("input[placeholder='Enter uint64[]...']"));
        assetIdInput.clear();
        assetIdInput.click();
        assetIdInput.sendKeys(ASSET_IDS);

        //Submit
        ClaimTreeSubmissionError result = claimTreeSubmission();


        sleep(5000);

 /*       // Click Cloud Wallet
        WebElement cloudWallet = locateElementByName("Cloud Wallet");
        cloudWallet.click();

        // Wait for popup
        sleep(4000);


        // Select the 2nd window
        String mainHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        Iterator<String> iterator = windowHandles.iterator();
        iterator.next();
        driver.switchTo().window(iterator.next());

        // Enter email
        WebElement emailInput = locateElementByCSS("input", "placeholder", "Email / Username");
        emailInput.click();
        emailInput.sendKeys("@gmail.com");

        // Enter password (very secure)
        WebElement passwordInput = locateElementByCSS("input", "placeholder", "Password");
        passwordInput.click();
        passwordInput.sendKeys("");

        WebElement loginButton = locateElementByName("Login");
        loginButton.click();
*/
//        driver.quit();
        return result;
    }
    public WebElement locateElementByCSS(String tag, String attr, String attrValue){
        sleep(400);
        return driver.findElement(By.cssSelector(tag + "["+attr+"='"+attrValue+"']"));
    }
    public WebElement locateElementByName(String contains){
        sleep(400);
        return driver.findElement(By.xpath("//*[contains(text(),'"+contains+"')]"));
    }
    public static void sleep(long ms){
        //todo stupid ass selenium workaround

        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
