package me.jakev.rafcr.persistentdata;

import java.io.*;

public class PersistentData {
    public long lastRan = 0;
    public int timesRan = 0;

    public static PersistentData initialize() {
        PersistentData data = new PersistentData();
        try {
            DataInputStream input = new DataInputStream(new FileInputStream("storage.dat"));
            data.lastRan = input.readLong();
            data.timesRan = input.readInt();
        } catch (FileNotFoundException e) {
            System.err.println("[PersistentData] storage.dat not found.");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return data;
    }

    public void save() {
        try {
            File file = new File("storage.dat");
            if (!file.exists()) {
                file.createNewFile();
                System.err.println("Created blank storage.dat");
            }
            DataOutputStream out = new DataOutputStream(new FileOutputStream("storage.dat"));
            System.err.println("Writing object: " + this);
            out.writeLong(lastRan);
            out.writeInt(timesRan);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "PersistentData{" +
                "lastRan=" + lastRan +
                ", timesRan=" + timesRan +
                '}';
    }
}
