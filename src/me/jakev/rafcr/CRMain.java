package me.jakev.rafcr;

import me.jakev.rafcr.persistentdata.PersistentData;
import me.jakev.rafcr.selenium.ChromeMain;

import java.util.Date;

public class CRMain {
    public static PersistentData data;
    public static void main(String[] args) {
        data = PersistentData.initialize();
        System.err.println("[CRMain] Starting main loop");
        while (true){
            System.err.println("[CRMain] Running main loop ========================");
            long currentTime = System.currentTimeMillis();
            System.err.println("[CRMain] Current System time: " + currentTime + " (" + new Date(currentTime) + ")");
            if(currentTime >= getNextClaimTree()){
                System.err.println("[CRMain] Running claimTree due to time being < next claim tree");
                ChromeMain cm = new ChromeMain();
                cm.spawnBrowser();
                cm.login();
                ChromeMain.ClaimTreeSubmissionError error = cm.claimTreeTextInput();
                if(error == ChromeMain.ClaimTreeSubmissionError.NONE || error == ChromeMain.ClaimTreeSubmissionError.CANNOT_CLAIM_YET){
                    data.timesRan++;
                    data.lastRan = currentTime;
                    if(data.timesRan == 10){
                        data.timesRan = 0;
                        cm.buyAndUseBooster();
                    }
                    data.save();
                }else if (error == ChromeMain.ClaimTreeSubmissionError.NEEDS_FERTILIZER){
                    ChromeMain.sleep(1000);
                    System.err.println("Applying booster due to error");
                    cm.buyAndUseBooster();
                    data.timesRan = 0;
                }else{
                    ChromeMain.sleep(1000);
                    cm.claimTreeSubmission();
                }
                cm.closeBrowser();
            }
            try {
                long timeLeftMs = (getNextClaimTree() - currentTime);
                long diffSeconds = timeLeftMs / 1000 % 60;
                long diffMinutes = timeLeftMs / (60 * 1000) % 60;
                long diffHours = timeLeftMs / (60 * 60 * 1000);
                System.err.println("[CRMain] Sleeping for 5 minutes. Next Update at: " + new Date(getNextClaimTree()));
                System.err.println("--->   This is in: " + diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds. [Ran: " + data.timesRan + "]");
                Thread.sleep(1000*60*5);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
    private static final long ONE_MINUTE = 1000*60;
    static long getNextClaimTree(){
        long lastRan = CRMain.data.lastRan;
        lastRan += ONE_MINUTE * 60 * 6; //60 seconds * 60 minutes * 6 hours + 1 minute
        lastRan += ONE_MINUTE;
        return lastRan;
    }
}
