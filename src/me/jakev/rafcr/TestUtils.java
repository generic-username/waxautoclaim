package me.jakev.rafcr;

import me.jakev.rafcr.selenium.ChromeMain;
import org.junit.jupiter.api.Test;

public class TestUtils {
    public static void main(String[] args) {
        ChromeMain cm = new ChromeMain();
        cm.spawnBrowser();
        cm.login();
        cm.buyAndUseBooster();
    }
    @Test
    public void loginTest(){
        ChromeMain cm = new ChromeMain();
        cm.spawnBrowser();
        cm.login();
    }

    @Test
    public void claimTest(){
        ChromeMain cm = new ChromeMain();
        cm.spawnBrowser();
        cm.login();
        cm.claimTreeTextInput();
        cm.claimTreeSubmission();
    }
}
